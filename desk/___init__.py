#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#

from bleak import BleakClient, BleakScanner
from bleak.backends.device import BLEDevice
from bleak.backends.scanner import AdvertisementData
from typing import Optional
import asyncio, logging

#
_UUID_COMMAND: str = "99fa0002-338a-1024-8a49-009c0215f78a"
_UUID_POSITION: str = "99fa0021-338a-1024-8a49-009c0215f78a"
_UUID_REFERENCE_INPUT: str = "99fa0031-338a-1024-8a49-009c0215f78a"
_UUID_ADV_SVC: str = "99fa0001-338a-1024-8a49-009c0215f78a"

#
_COMMAND_DOWN: bytearray = bytearray([0x46, 0x00])
_COMMAND_STOP: bytearray = bytearray([0xFF, 0x00])
_COMMAND_UP: bytearray = bytearray([0x47, 0x00])
_COMMAND_REFERENCE_INPUT_STOP: bytearray = bytearray([0x01, 0x80])

class Desk:
	"""
	"""

	# Minimum desk height in metres.
	LOWER_LIMIT: float = 0.62

	# Maximum desk height in metres.
	UPPER_LIMIT: float = 1.27

	def __init__(self, address: str) -> None:
		"""
		"""
		self._client = BleakClient(address)

		self._logger = logging.basicConfig()

	async def __aenter__(self):
		"""
		"""
		try:
			await self._client.__aenter__()
		except Exception:
			self._logger.critical('Failed to connect to the desk')
		return self

	async def __aexit__(self, *args, **kwargs) -> Optional[bool]:
		"""
		"""
		return await self._client.__aexit__(*args, **kwargs)

	def _bytes_to_metres(self, raw: bytearray) -> float:
		"""
		Converts the byte value received from the desk into metres.
		"""
		integer = (int(raw[1]) << 8) + int(raw[0])

		return float(integer / 10000) + self.LOWER_LIMIT

	@staticmethod
	def _is_desk(device: BLEDevice, data: AdvertisementData) -> bool:
		"""
		"""
		return _UUID_ADV_SVC in data.service_uuids

	@staticmethod
	async def discover() -> Optional[str]:
		"""
		Tries to find the desk's MAC address through discovery of connected devices.
		"""
		try:
			device = await BleakScanner.find_device_by_filter(_is_desk)
		except Exception:
			return None

		if device is None:
			return None

		return device.address

	async def get_position(self) -> float:
		"""
		"""
		return self._bytes_to_metres(await self._client.read_gatt_char(_UUID_POSITION))

	async def move_down(self) -> None:
		"""
		Moves the desk downwards.
		"""
		await self._client.write_gatt_char(_UUID_COMMAND, _COMMAND_DOWN, response=False)

	async def move_to(self, target: float) -> None:
		"""
		Moves the desk to specified position.
		"""
		if target > self.UPPER_LIMIT:
			raise ValueError(f'{target:.3f} exceeds upper limit {self.UPPER_LIMIT:.3f}')

		if target < self.LOWER_LIMIT:
			raise ValueError(f'{target:.3f} exceeds lower limit {self.LOWER_LIMIT:.3f}')

		previous_position = await self.get_position()
		moving_up = target > previous_position

		while True:
			position = await self.get_position()
			difference = target - position

			self._logger.debug(f'{target=} {position=} {difference=}')

			if (position < previous_position and moving_up) or (position > previous_position and not moving_up):
				self._logger.warning('Desk safety feature kicked in')
				return

			if abs(difference) < 0.005:
				self._logger.info(f'Reached target of {target:.3f}')
				await self.stop()
				return

			elif difference < 0:
				await self.move_down()

			elif difference > 0:
				await self.move_up()

			previous_position = position

	async def move_up(self) -> None:
		"""
		Moves the desk upwards.
		"""
		await self._client.write_gatt_char(_UUID_COMMAND, _COMMAND_UP, response=False)

	async def stop(self) -> None:
		"""
		Stops desk movement.
		"""
		await asyncio.gather(
			self._client.write_gatt_char(_UUID_COMMAND, _COMMAND_STOP, response=False),
			self._client.write_gatt_char(_UUID_REFERENCE_INPUT, _COMMAND_REFERENCE_INPUT_STOP, response=False),
		)
