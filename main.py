#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#

from argparse import ArgumentParser
from desk import Desk
import asyncio
import sys

async def position() -> None:
	"""
	"""
	async with Desk('E0:45:30:82:EA:A5') as desk:
		data = await desk.get_position()
		print(f"{data:.3f} metres")

async def move_to(target: float) -> None:
	"""
	"""
	async with Desk('E0:45:30:82:EA:A5') as desk:
		await desk.move_to(target)

def main(arguments: Optional[List[str]]) -> int:
	"""
	"""
	parser = ArgumentParser(description='IKEA Idåsen desk control application')

	# Populate the argument parser
	parser.add__parser('position', help='Get the current poistion of the desk')

	# Run the parser and return given arguments
	arguments = parser.parse_args(arguments)

	# Run the specified function
	result = asyncio.run(function(arguments))

	# If no result were returned - interpret as 0
	if result is None:
		result = 0

	return result

if __name__ == '__main__':
	sys.exit(main(sys.argv))
